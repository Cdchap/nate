<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Str;

class MobileNav extends Component
{
    public $name;
    public $email;
    public $initial;

    public function mount()
    {
        $this->name = auth()->user()->name;
        $this->email = auth()->user()->email;
        $this->initial = Str::upper(Str::substr($this->name, 0, 1));
    }

    public function render()
    {
        return view('livewire.mobile-nav');
    }
}
