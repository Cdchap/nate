<?php

namespace App\Http\Livewire;

use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithPagination;
use App\Project;

class ProjectIndex extends Component
{
    use WithPagination;

    protected $updatesQueryString = ['search' => ['except' => '']];

    public $search ='';
    public $perPage = 10;

    public function render()
    {
        return view('livewire.project-index', [
            'projects' => Project::search(['name'], $this->search)
                        ->orderBy('created_at','desc')
                        ->paginate($this->perPage),
        ]);
    }
}
