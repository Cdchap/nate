<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Project;

class Work extends Component
{
    use WithPagination;

    public function render()
    {
        return view('livewire.work', [
            'projects' => Project::latest()
                                ->paginate(5)
        ]);
    }
}
