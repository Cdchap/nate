<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Contact extends Component
{
    public $name;
    public $email;
    public $message;

    
    public function render()
    {
        return view('livewire.contact');
    }
}
