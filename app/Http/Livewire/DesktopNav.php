<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Str;

class DesktopNav extends Component
{
    public $initial;

    public function mount()
    {
        $this->initial = Str::upper(Str::subStr(auth()->user()->name, 0,1));
    }

    public function render()
    {
        return view('livewire.desktop-nav');
    }
}
