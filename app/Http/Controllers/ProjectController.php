<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Project;

class ProjectController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();

        $validatedItems = $request->validate([
            'name' => 'required',
            'country' => 'required',
            'address' =>'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'description' => 'required'
        ]);


        if (!$validatedItems) {
            return back()
                    ->withErrors()
                    ->withInput();
        } else {

            $finalItems = Arr::add($validatedItems, 'user_id', $user->id);
            $project = Project::create($finalItems);

        }

        if($request->hasFile('before_photo')) {
            $validatedBeforePhoto = $request->validate([
                'before_photo' => 'required|mimes:jpeg'
            ]);

            if(!$validatedBeforePhoto) {
                return redirect('project.create')
                        ->withErrors()
                        ->withInput();
            } else {
                $project->addMediaFromRequest('before_photo')->toMediaCollection('before_photos');
            }
        }

        if($request->hasFile('after_photo')) {
            $validateAfterPhoto = $request->validate([
                'after_photo' => 'required|mimes:jpeg'
            ]);

            if(!$validateAfterPhoto) {
                return redirect('project.create')
                        ->withErrors()
                        ->withInput();
            } else {
                $project->addMediaFromRequest('after_photo')->toMediaCollection('after_photos');
            }
        }

        session()->flash('success', 'A project has been successfully added.');
        return redirect()->route('project.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        $project = Project::findOrFail($project->id);

        return view('projects.show', compact(['project']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        $project = Project::findOrFail($project->id);

        return view('projects.edit', compact(['project']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $project = Project::findOrFail($id);

       $validatedItems = $request->validate([
            'name' => 'required',
            'country' => 'required',
            'address' =>'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'description' => 'required'
        ]);

        if (!$validatedItems) {
            return redirect('project.edit')
                    ->withErrors()
                    ->withInput();
        } else {
            if($request->hasFile('before_photo')) {

                $validatedBefore = $request->validate([
                    'before_photo' => 'mimes:jpeg'
                ]);

                $mediaItems = $project->getMedia('before_photos');
                $mediaItems[0]->delete();

                $project->addMediaFromRequest('before_photo')->toMediaCollection('before_photos');

            }
            if($request->hasFile('after_photo')) {
                $validatedAfter = $request->validate([
                    'after_photo' => 'mimes:jpeg'
                ]);

                $mediaItems = $project->getMedia('after_photos');
                $mediaItems[0]->delete();

                $project->addMediaFromRequest('after_photo')->toMediaCollection('after_photos');
            }

//            $project->name = $validatedItems['name'];
//            $project->country = $validatedItems['country'];
//            $project->address = $validatedItems['address'];
//            $project->city = $validatedItems['city'];
//            $project->state = $validatedItems['state'];
//            $project->zip = $validatedItems['zip'];
//            $project->description = $validatedItems['description'];
            $project->save($validatedItems);

            session()->flash('success', 'A project has been successfully updated.');
            return redirect()->route('project.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::findOrFail($id);

        $project->delete();

        session()->flash('success', 'The project has been deleted');

        return redirect()->route('project.index');
    }
}
