<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Project extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $guarded = [];

    public function registerMediaConversions(Media $media = null): void
    {

        $this->addMediaConversion('thumb')
            ->width(100)
            ->height(75)
            ->sharpen(10);

        $this->addMediaConversion('medium')
            ->width(600)
            ->height(450)
            ->sharpen(10);
    }

    public function user ()
    {
        return $this->belongsTo('App\User');
    }

}
