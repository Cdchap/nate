<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::livewire('/', 'welcome')->name('welcome');
Route::livewire('/about', 'about')->name('about');
Route::livewire('/contact', 'contact')->name('contact');
Route::livewire('/work', 'work')->name('work');

Auth::routes();

Route::middleware('auth')->group(function() {
    Route::get('admin/dashboard', 'HomeController@index')->name('dashboard');

    Route::livewire('admin/projects', 'project-index')->layout('layouts.admin')->name('project.index');
    Route::get('admin/projects/create', 'ProjectController@create')->name('project.create');
    Route::delete('adming/projects/{project}', 'ProjectController@destroy')->name('project.delete');
    Route::get('admin/projects/{project}', 'ProjectController@show')->name('project.show');
    Route::get('admin/projects/{project}/edit', 'ProjectController@edit')->name('project.edit');
    Route::post('admin/projects/create', 'ProjectController@store')->name('project.store');
    Route::put('admin/projects/{project}', 'ProjectController@update')->name('project.update');
});



