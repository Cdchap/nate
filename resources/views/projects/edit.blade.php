@extends('layouts.admin')

@section('heading', 'Edit: ' . $project->name )

@section('content')

    <div class="md:grid md:grid-cols-3 md:gap-6">
        <div class="md:col-span-1">
            <h3 class="text-lg font-medium leading-6 text-gray-900">Edit Project</h3>
            <p class="mt-1 text-sm leading-5 text-gray-500">
                Edit project details and information
            </p>
        </div>
        <div x-data="{ modalOpen: false }" class="mt-5 md:mt-0 md:col-span-2">
            <form action="{{ route('project.update', $project) }}" method="POST" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="grid grid-cols-6 gap-6 mt-8">
                    <div class="relative col-span-6 sm:col-span-3">
                        <label for="name" class="block text-sm font-medium leading-5 text-gray-700">Name</label>
                        <input id="name" name="name" class="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md @error('name') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red @enderror shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5" value="{{ $project->name }}"/>
                        @error('name')
                        <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                            <svg class="h-5 w-5 text-red-500" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        @enderror
                        @error('name')
                        <div class="col-span-6 sm:col-span-3 text-sm text-red-600 flex flex-col justify-center">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-span-6 sm:col-span-3">
                        <label for="country" class="block text-sm font-medium leading-5 text-gray-700">Country / Region</label>
                        <select id="country" name="country" class="mt-1 block form-select w-full py-2 px-3 py-0 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                            <option>United States</option>
                            <option>Canada</option>
                            <option>Mexico</option>
                        </select>
                    </div>

                    <div class="relative col-span-6">
                        <label for="address" class="block text-sm font-medium leading-5 text-gray-700">Street address</label>
                        <input id="address" name="address" class="mt-1 form-input  @error('address') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red @enderror block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5" value="{{ $project->address }}" />
                        @error('address')
                        <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                            <svg class="h-5 w-5 text-red-500" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        @enderror
                        @error('address')
                        <div class="col-span-6 sm:col-span-3 text-sm text-red-600 flex flex-col justify-center">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="relative col-span-6 sm:col-span-6 lg:col-span-2">
                        <label for="city" class="block text-sm font-medium leading-5 text-gray-700">City</label>
                        <input id="city" name="city" class="mt-1 form-input  @error('address') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red @enderror block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5" value="{{ $project->city }}"/>
                        @error('city')
                        <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                            <svg class="h-5 w-5 text-red-500" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        @enderror
                        @error('city')
                        <div class="col-span-6 sm:col-span-3 text-sm text-red-600 flex flex-col justify-center">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="relative col-span-6 sm:col-span-3 lg:col-span-2">
                        <label for="state" class="block text-sm font-medium leading-5 text-gray-700">State / Province</label>
                        <select id="state" name="state" class="mt-1 block form-select w-full py-2 px-3 py-0 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                            <option value="{{ $project->state }}">{{ $project->state }}</option>
                            <option value="AL">Alabama</option>
                            <option value="AK">Alaska</option>
                            <option value="AZ">Arizona</option>
                            <option value="AR">Arkansas</option>
                            <option value="CA">California</option>
                            <option value="CO">Colorado</option>
                            <option value="CT">Connecticut</option>
                            <option value="DE">Delaware</option>
                            <option value="DC">District of Columbia</option>
                            <option value="FL">Florida</option>
                            <option value="GA">Georgia</option>
                            <option value="HI">Hawaii</option>
                            <option value="ID">Idaho</option>
                            <option value="IL">Illinois</option>
                            <option value="IN">Indiana</option>
                            <option value="IA">Iowa</option>
                            <option value="KS">Kansas</option>
                            <option value="KY">Kentucky</option>
                            <option value="LA">Louisiana</option>
                            <option value="ME">Maine</option>
                            <option value="MD">Maryland</option>
                            <option value="MA">Massachusetts</option>
                            <option value="MI">Michigan</option>
                            <option value="MN">Minnesota</option>
                            <option value="MS">Mississippi</option>
                            <option value="MO">Missouri</option>
                            <option value="MT">Montana</option>
                            <option value="NE">Nebraska</option>
                            <option value="NV">Nevada</option>
                            <option value="NH">New Hampshire</option>
                            <option value="NJ">New Jersey</option>
                            <option value="NM">New Mexico</option>
                            <option value="NY">New York</option>
                            <option value="NC">North Carolina</option>
                            <option value="ND">North Dakota</option>
                            <option value="OH">Ohio</option>
                            <option value="OK">Oklahoma</option>
                            <option value="OR">Oregon</option>
                            <option value="PA">Pennsylvania</option>
                            <option value="RI">Rhode Island</option>
                            <option value="SC">South Carolina</option>
                            <option value="SD">South Dakota</option>
                            <option value="TN">Tennessee</option>
                            <option value="TX">Texas</option>
                            <option value="UT">Utah</option>
                            <option value="VT">Vermont</option>
                            <option value="VA">Virginia</option>
                            <option value="WA">Washington</option>
                            <option value="WV">West Virginia</option>
                            <option value="WI">Wisconsin</option>
                            <option value="WY">Wyoming</option>
                        </select>
                        @error('state')
                        <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                            <svg class="h-5 w-5 text-red-500" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        @enderror
                        @error('state')
                        <div class="col-span-6 sm:col-span-3 text-sm text-red-600 flex flex-col justify-center">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="relative col-span-6 sm:col-span-3 lg:col-span-2">
                        <label for="zip" class="block text-sm font-medium leading-5 text-gray-700">ZIP / Postal</label>
                        <input id="zip" name="zip" class="mt-1 form-input  @error('zip') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red @enderror block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5" value="{{ $project->zip }}"/>
                        @error('zip')
                        <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                            <svg class="h-5 w-5 text-red-500" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        @enderror
                        @error('zip')
                        <div class="col-span-6 sm:col-span-3 text-sm text-red-600 flex flex-col justify-center">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="relative mt-6">
                    <label for="about" class="block text-sm leading-5 font-medium text-gray-700">
                        Description
                    </label>
                    <div class="rounded-md shadow-sm">
                        <textarea id="about" name="description" rows="3" class="form-textarea @error('description') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red @enderror mt-1 block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5" placeholder="Description here">{{ $project->description }}</textarea>
                    </div>
                    @error('description')
                    <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                        <svg class="h-5 w-5 text-red-500" fill="currentColor" viewBox="0 0 20 20">
                            <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clip-rule="evenodd" />
                        </svg>
                    </div>
                    @enderror
                    @error('description')
                    <div class="col-span-6 sm:col-span-3 text-sm text-red-600 flex flex-col justify-center">{{ $message }}</div>
                    @enderror
                    <p class="mt-2 text-sm text-gray-500">
                        Brief description of the project.
                    </p>
                </div>
                <div class="grid grid-cols-6 gap-6 mt-8">
                    <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                        <h3 class="block text-sm font-medium leading-5 text-gray-700">Before Photo</h3>
                        @if($project->getMedia('before_photos')->first())
                            <img src="{{ $project->getMedia('before_photos')->first()->getUrl('thumb') }}" alt="{{ $project->name }}" srcset="">
                        @endif
                    </div>
                    <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                        <h3 class="block text-sm font-medium leading-5 text-gray-700">After Photo</h3>
                        @if($project->getMedia('after_photos')->first())
                            <img src="{{ $project->getMedia('after_photos')->first()->getUrl('thumb') }}" alt="{{ $project->name }}" srcset="">
                        @endif
                    </div>
                </div>
                <div class="grid grid-cols-6 gap-6 mt-4">
                    <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                        <label for="before_photo" class="block text-sm font-medium leading-5 text-gray-700">Change</label>
                        <input type="file" id="before_photo" name="before_photo" class="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5" file/>
                    </div>
                    <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                        <label for="after_photo" class="block text-sm font-medium leading-5 text-gray-700">Change</label>
                        <input type="file" id="after_photo" name="after_photo" class="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5" file/>
                    </div>
                </div>
                <div>
            <span class="inline-flex rounded-md shadow-sm mt-8">
                <button type="submit" class="inline-flex items-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition ease-in-out duration-150">
                    Edit Project
                </button>
            </span>
                    <span  class="inline-flex rounded-md shadow-sm mt-8">
                <button @click="modalOpen = true" type="button" class="inline-flex items-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-red-600 hover:bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-red active:bg-red-700 transition ease-in-out duration-150">
                    Delete Project
                </button>
            </span>
                </div>
            </form>
            {{--    Modal for deleting photo, x-data defined at top level div    --}}
            <div  x-show="modalOpen" class="fixed bottom-0 inset-x-0 px-4 pb-4 sm:inset-0 sm:flex sm:items-center sm:justify-center">
                <div x-show="modalOpen" x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100" x-transition:leave="ease-in duration-200" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" class="fixed inset-0 transition-opacity">
                    <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
                </div>

                <div x-show="modalOpen" x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95" x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100" x-transition:leave="ease-in duration-200" x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100" x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95" class="bg-white rounded-lg px-4 pt-5 pb-4 overflow-hidden shadow-xl transform transition-all sm:max-w-lg sm:w-full sm:p-6">
                    <div class="sm:flex sm:items-start">
                        <div class="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-red-100 sm:mx-0 sm:h-10 sm:w-10">
                            <svg class="h-6 w-6 text-red-600" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"/>
                            </svg>
                        </div>
                        <div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                            <h3 class="text-lg leading-6 font-medium text-gray-900">
                                Delete Project
                            </h3>
                            <div class="mt-2">
                                <p class="text-sm leading-5 text-gray-500">
                                    Are you sure you want to delete the photo titled <span class="font-bold italic">{{ $project->name }}</span>?
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="mt-5 sm:mt-4 sm:ml-10 sm:pl-4 sm:flex">
                        <form action="{{ route('project.delete', $project) }}" method="POST">
                            @method('DELETE')
                            @csrf
                            <span class="flex w-full rounded-md shadow-sm sm:w-auto">
                            <button type="submit" class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-medium text-white shadow-sm hover:bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-red transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Delete
                            </button>
                        </span>
                        </form>
                        <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:ml-3 sm:w-auto">
                        <button @click="modalOpen = false" type="button" class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700 shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                        Cancel
                        </button>
                    </span>
                    </div>
                </div>
            </div>
            {{-- End of modal --}}
        </div>
    </div>

@endsection
