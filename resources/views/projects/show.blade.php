@extends('layouts.admin')

@section('heading', $project->name)

@section('content')

<div class="">
  <div class="px-4 py-5 border-b border-gray-200 sm:px-6">
    <h3 class="text-lg leading-6 font-medium text-gray-900">
     Project Sheet
    </h3>
    <p class="mt-1 max-w-2xl text-sm leading-5 text-gray-500">
      Project details and information.
    </p>
  </div>
  <div class="px-4 py-5 sm:p-0">
    <dl>
      <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5">
        <dt class="text-sm leading-5 font-medium text-gray-500">
         Project Name
        </dt>
        <dd class="mt-1 text-sm leading-5 text-gray-900 sm:mt-0 sm:col-span-2">
           {{ $project->name }}
        </dd>
      </div>
      <div class="mt-8 sm:mt-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:border-t sm:border-gray-200 sm:px-6 sm:py-5">
        <dt class="text-sm leading-5 font-medium text-gray-500">
          Address
        </dt>
        <dd class="mt-1 text-sm leading-5 text-gray-900 sm:mt-0 sm:col-span-2">
          {{ $project->address }} {{ $project->city }}, {{ $project->state }} {{ $project->zip }}
        </dd>
      </div>
      <div class="mt-8 sm:mt-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:border-t sm:border-gray-200 sm:px-6 sm:py-5">
        <dt class="text-sm leading-5 font-medium text-gray-500">
          Description
        </dt>
        <dd class="mt-1 text-sm leading-5 text-gray-900 sm:mt-0 sm:col-span-2">
            {{ $project->description }}
        </dd>
      </div>
      <div class="mt-8 sm:mt-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:border-t sm:border-gray-200 sm:px-6 sm:py-5">
        <dt class="text-sm leading-5 font-medium text-gray-500">
          Associated Photos
        </dt>
        <dd class="mt-1 text-sm leading-5 text-gray-900 sm:mt-0 sm:col-span-2">
          <ul class="border border-gray-200 rounded-md">
            <li class="pl-3 pr-4 py-3 flex items-center justify-between text-sm leading-5">
              <div class="w-0 flex-1 flex items-center">
                  @if($project->getMedia('before_photos')->first())
                      <img src="{{ $project->getMedia('before_photos')->first()->getUrl('thumb') }}" alt="{{ $project->name }}" srcset="">
                  @endif
              </div>
              <div class="ml-4 flex-shrink-0">
                <a href="{{ route('project.edit', $project) }}" class="font-medium text-indigo-600 hover:text-indigo-500 transition duration-150 ease-in-out">
                  Edit
                </a>
              </div>
            </li>
            <li class="border-t border-gray-200 pl-3 pr-4 py-3 flex items-center justify-between text-sm leading-5">
              <div class="w-0 flex-1 flex items-center">
                  @if($project->getMedia('after_photos')->first())
                      <img src="{{ $project->getMedia('after_photos')->first()->getUrl('thumb') }}" alt="{{ $project->name }}" srcset="">
                  @endif()
              </div>
              <div class="ml-4 flex-shrink-0">
                <a href="{{ route('project.edit', $project) }}" class="font-medium text-indigo-600 hover:text-indigo-500 transition duration-150 ease-in-out">
                  Edit
                </a>
              </div>
            </li>
          </ul>
        </dd>
      </div>
    </dl>
  </div>
</div>

@endsection
