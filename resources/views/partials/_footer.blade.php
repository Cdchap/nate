
<div class="border-t">
    <div class="container pl-8 lg:mx-auto h-28 flex items-center">
        <ul class="flex">
            <li class="mr-2 sm:mr-4 text-gray-500 text-sm font-bold hover:text-black hover:font-bold"><a href="{{ route('welcome') }}">NDN</a><li>
            <li class="mx-4 sm:mx-6 text-gray-500 text-sm font-light hover:text-black hover:font-normal"><a href="{{ route('about') }}">About</a></li>
            <li class="mx-4 sm:mx-6 text-gray-500 text-sm font-light hover:text-black hover:font-normal"><a href="{{ route('work') }}">Work</a></li>
            <li class="ml-4 sm:ml-6 text-gray-500 text-sm font-light hover:text-black hover:font-normal"><a href="{{ route('contact') }}">Contact</a></li>
            <li class="ml-6 sm:ml-10 text-gray-500 text-sm font-light hover:text-black hover:font-normal"><a href="{{ route('login') }}">Login</a></li>
        </ul>
    </div>
</div>
