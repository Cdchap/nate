

<button
    @click= " navShow = true "
    x-on:mouseenter= " navShow = true "
    class="font-bold font text-gray-300 text-2xl">Menu
</button>

<div x-show="navShow"
     style="display: none"
    @click.away="navShow = false"
    x-on:mouseleave = " navShow = false "
    x-transition:enter="transition ease-out duration-100"
    x-transition:enter-start="opacity-0 transform -translate-x-full"
    x-transition:enter-end="opacity-100 transform -translate-x-0"
    x-transition:leave="transition ease-in duration-100"
    x-transition:leave-start="opacity-100 transform translate-x-full"
    x-transition:leave-end="opacity-0 transform -translate-x-0"
    class="bg-white pl-4 pr-6 py-4 -ml-8 rounded">
    <ul x-data="{ homeChev: false, aboutChev: false, workChev: false}"
            class="flex flex-col">

        <li class="text-xl font-medium">
            <a href="{{ route('welcome') }}"
                x-on:mouseenter="homeChev = true"
                x-on:mouseleave="homeChev = false">
                <div class="-ml-2 flex items-center">
                    <div class="h-6 w-6">
                        <span class="h-6 w-6 bg-pink-200"
                            x-show="homeChev"
                            x-transition:enter="transition ease-out duration-100"
                            x-transition:enter-start="opacity-0 transform scale-90"
                            x-transition:enter-end="opacity-100 transform scale-100"
                            x-transition:leave="transition ease-in duration-100"
                            x-transition:leave-start="opacity-100 transform scale-100"
                            x-transition:leave-end="opacity-0 transform scale-90" >
                                <x:heroicon-s-cheveron-right class="w-6 h-6 text-black"/>
                        </span>
                    </div>
                    <span>Home</span>
                </div>
            </a>
        </li>

        <li class="text-xl font-medium">
            <a href="{{ route('about') }}"
                x-on:mouseenter="aboutChev = true"
                x-on:mouseleave="aboutChev = false">
                <div class="-ml-2 flex items-center">
                    <div class="h-6 w-6">
                        <span class="h-6 w-6 bg-pink-200"
                            x-show="aboutChev"
                            x-transition:enter="transition ease-out duration-100"
                            x-transition:enter-start="opacity-0 transform scale-90"
                            x-transition:enter-end="opacity-100 transform scale-100"
                            x-transition:leave="transition ease-in duration-100"
                            x-transition:leave-start="opacity-100 transform scale-100"
                            x-transition:leave-end="opacity-0 transform scale-90" >
                                <x:heroicon-s-cheveron-right class="w-6 h-6 text-black"/>
                        </span>
                    </div>
                    <span>About</span>
                </div>
            </a>
        </li>

        <li class="text-xl font-medium">
            <a href="{{ route('work') }}"
                x-on:mouseenter="workChev = true"
                x-on:mouseleave="workChev = false">
                <div class="-ml-2 flex items-center">
                    <div class="h-6 w-6">
                        <span class="h-6 w-6 bg-pink-200"
                            x-show="workChev"
                            x-transition:enter="transition ease-out duration-100"
                            x-transition:enter-start="opacity-0 transform scale-90"
                            x-transition:enter-end="opacity-100 transform scale-100"
                            x-transition:leave="transition ease-in duration-100"
                            x-transition:leave-start="opacity-100 transform scale-100"
                            x-transition:leave-end="opacity-0 transform scale-90" >
                                <x:heroicon-s-cheveron-right class="w-6 h-6 text-black"/>
                        </span>
                    </div>
                    <span>Work</span>
                </div>
            </a>
        </li>
    </ul>
</div>
