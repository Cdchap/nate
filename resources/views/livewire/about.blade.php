<div>
    <section class="vh90 max-w-7xl mx-auto mb-4 sm:mb-0 px-4 sm:px-6 lg:px-8">
        <div class="max-w-3xl mx-auto grid grid-col-1 grid-rows-3">
            <h1 class="row-start-2 row-end-3 text-6xl font-bold tracking-tight">About</h1>
            <p class="row-start-3 row-end-4 text-xl sm:text-2xl font-light">
                <span class="font-bold">A professional portfolio manager to help your properties succeed.</span>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id euismod metus, sed elementum urna.
                Cras placerat interdum tortor a luctus. Praesent erat dolor, egestas id orci ac, placerat pretium elit.
                Ut efficitur velit at ultrices dictum. Aenean et tristique mi, vel lacinia ligula.
            </p>
        </div>
    </section>
    <section class="sm:h-96 h-80 bg-black border-b">
        <div class="max-w-3xl mx-auto h-full relative">
            <img class="absolute left-0 bottom-0 w-full h-auto"
                src="{{ asset('images/3-houses.svg') }}" alt="">
        </div>
    </section>
    <section class="my-16 px-4 sm:px-6 lg:px-8">
        <div class="max-w-3xl mx-auto">

            <h3 class="font-bold text-xl sm:text-2xl mb-4">Experience</h3>
            <p class="font-light text-xl sm:text-2xl">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Et pharetra pharetra massa massa. Elementum integer enim neque volutpat ac
                tincidunt. Ut etiam sit amet nisl. Ut ornare lectus sit amet. Neque vitae tempus quam pellentesque nec.
                Est ante in nibh mauris. In ante metus dictum at. Euismod quis viverra nibh cras pulvinar mattis nunc
                sed blandit. Odio tempor orci dapibus ultrices in iaculis. Tempor orci dapibus ultrices in iaculis nunc
                sed augue. Est pellentesque elit ullamcorper dignissim cras. Purus gravida quis blandit turpis cursus
                in. Lobortis mattis aliquam faucibus purus in massa. Porta non pulvinar neque laoreet suspendisse
                interdum. Egestas congue quisque egestas diam. Urna porttitor rhoncus dolor purus non enim praesent
                elementum. Netus et malesuada fames ac turpis egestas maecenas pharetra convallis. Suscipit adipiscing
                bibendum est ultricies integer quis auctor elit sed.
            </p>
        </div>
    </section>
    <section class="sm:h-96 h-80 bg-black border-b py-10">
        <div class="max-w-3xl mx-auto h-full flex flex-col justify-center items-center mt-4">
            <img class=""
                src="{{ asset('images/rent.svg') }}" alt="building">
        </div>
    </section>
    <section class="my-16 px-4 sm:px-6 lg:px-8">
        <div class="max-w-3xl mx-auto">

            <h3 class="font-bold text-xl sm:text-2xl mb-4">Reliability</h3>
            <p class="font-light text-xl sm:text-2xl">
                Sed cras ornare arcu dui vivamus arcu felis bibendum. Duis at consectetur lorem donec massa. Egestas sed
                tempus urna et pharetra pharetra massa massa. Urna id volutpat lacus laoreet. Aliquam id diam maecenas
                ultricies mi eget. Enim blandit volutpat maecenas volutpat blandit aliquam etiam erat. Cursus sit amet
                dictum sit. Maecenas volutpat blandit aliquam etiam erat velit scelerisque. Feugiat nibh sed pulvinar
                proin gravida hendrerit lectus a. Ac tortor vitae purus faucibus. Metus dictum at tempor commodo.
                Ultrices gravida dictum fusce ut. Netus et malesuada fames ac turpis. Sodales ut etiam sit amet. Arcu
                dui vivamus arcu felis bibendum ut tristique. Leo duis ut diam quam.
            </p>
        </div>
    </section>
    <section class=" bg-black ">
        <div class="container lg:mx-auto pl-8 lg:pl-0 h-64 flex flex-col justify-center">
            <h3 class="text-white text-xl mb-4">Ready to work together? Perfect.</h3>
            <div>
                <a href="{{ route('contact') }}" class="text-xl text-white font-semibold border-b-4 border-white hover:border-green-300 hover:text-green-300 pb-1">
                <span class="inline-flex">
                    Contact <x:heroicon-s-cheveron-right class="w-6 h-6 mt-1 ml-1 -mr-2"/>
                </span>
                </a>
            </div>
        </div>
    </section>
</div>
