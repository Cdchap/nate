<div>
    <section class="min-w-full vh90 relative">
        <div class="absolute bottom-0 left-0 max-w-sm ml-12 mb-8">
            <h1 class="text-3xl leading-9 tracking-tight font-bold mb-4">Next Door Nate</h1>
            <h2 class="font-light text-xl mb-4">A professional portfolio manager to help your properties succeed.</h2>
            <a href="{{ route('contact') }}" class="text-xl font-semibold border-b-4 border-black hover:border-red-800 hover:text-red-800 pb-1">
                <span class="inline-flex">
                    Contact <x:heroicon-s-cheveron-right class="w-6 h-6 mt-1 ml-1 -mr-2"/>
                </span>
            </a>
        </div>

    </section>
    <section class="sm:h-96 h-80 bg-black border-b">
        <div class="relative h-full w-full max-w-screen-xl">
            @include('partials.svg._housesGreen')
        </div>
    </section>

    <section class="grid grid-cols-3 grid-rows-none gap-6 px-8 max-w-screen-xl mb-8">
        <div class="col-span-3 mt-20 mb-8">
            <h3 class="text-3xl leading-9 font-bold text-gray-900">Why Next Door Nate?</h3>
        </div>
        <div class="col-span-3 md:col-span-1">
            <h4 class="text-xl font-medium text-gray-900 mb-4">Experience</h4>
            <p class="text-gray-800 font-normal"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</strong> Nullam dui nunc, euismod nec lectus at, pharetra iaculis neque. Pellentesque tempor sed arcu et pellentesque. Curabitur eget sem id est consectetur aliquam. Ut at nunc pharetra, faucibus massa sed, convallis ante. Nam vel risus ac tellus malesuada hendrerit a sit amet ligula. Proin tristique lorem libero, vitae vulputate tortor dignissim in. Nam et nisl fringilla, condimentum felis ut, luctus ligula. Aliquam erat volutpat. Maecenas non arcu imperdiet, interdum urna a, sagittis turpis. </p>
        </div>
        <div class="col-span-3 md:col-span-1">
            <h4 class="text-xl font-medium text-gray-900 mb-4">Quality</h4>
            <p class="text-gray-800 font-normal"><strong>Quisque tellus magna, congue at tincidunt sodales, auctor sed urna.</strong> Maecenas quis ligula pulvinar, vehicula enim id, sodales est. In nunc libero, fringilla ac libero in, interdum efficitur purus. Maecenas finibus purus lacus, malesuada interdum lacus ultrices ut. Maecenas ac lorem ac odio vulputate sagittis. In vulputate nibh tellus, eu elementum mauris semper vel. Mauris ac tempus ex. Aenean euismod risus a neque pharetra rutrum. Nunc vel justo at magna feugiat facilisis. Maecenas faucibus, orci nec pellentesque ultricies, lectus orci porttitor lacus, nec luctus magna neque vitae nulla. </p>
        </div>
        <div class="col-span-3 md:col-span-1">
            <h4 class="text-xl font-medium text-gray-900 mb-4">Buzzword</h4>
            <p class="text-gray-800 font-normal"><strong>Quisque placerat elit id risus posuere accumsan.</strong> Nunc ut sapien lorem. In sagittis rhoncus tellus quis mollis. In blandit viverra risus vel accumsan. Nulla semper eros sed ante placerat, ornare bibendum dui ultricies. Curabitur volutpat, nunc quis gravida semper, mi tellus facilisis dui, eget vestibulum leo nisl ac nisi. Sed mi nibh, gravida eu viverra sed, viverra facilisis sapien. Fusce in dolor fringilla, consequat nisi vitae, tincidunt eros. Donec bibendum sit amet lorem et tempus. Morbi sed sagittis libero, et commodo ligula. Cras vestibulum justo nec magna fermentum, in imperdiet lorem mattis. Praesent vitae nulla lectus. </p>
        </div>
        <div class="col-span-3">
            <div class="mt-8 mb-8">
                <a href="{{ route('about') }}" class="text-xl font-semibold border-b-4 border-black hover:border-red-800 hover:text-red-800 pb-1">
                    <span class="inline-flex">
                        Learn more <x:heroicon-s-cheveron-right class="w-6 h-6 mt-1 ml-1 -mr-2"/>
                    </span>
                </a>
            </div>
        </div>
    </section>
    <section class=" bg-black ">
        <div class="container lg:mx-auto pl-8 lg:pl-0 h-64 flex flex-col justify-center">
            <h3 class="text-white text-xl mb-4">Ready to work together? Perfect.</h3>
            <div>
                <a href="{{ route('contact') }}" class="text-xl text-white font-semibold border-b-4 border-white hover:border-green-300 hover:text-green-300 pb-1">
                <span class="inline-flex">
                    Contact <x:heroicon-s-cheveron-right class="w-6 h-6 mt-1 ml-1 -mr-2"/>
                </span>
                </a>
            </div>
        </div>
    </section>
</div>


