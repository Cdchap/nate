<article class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
    <div class="max-w-3xl mx-auto pt-64">
        <h2 class="text-5xl sm:text-6xl font-bold tracking-tight">Work</h2>
        <p class="text-xl text-gray-400 font-light leading-3">A selection of work from my portfolio</p>
        <div class="flex justify-between mt-16">
            <h3 class="font-bold text-xl text-center">Before &xrarr; After</h3>
        </div>
        @foreach( $projects as $project)
            <section class="mb-8 sm:mb-16">
                <div class="grid grid-cols-2 gap-4 py-4">
                    <div class="shadow-md rounded overflow-hidden">
                        @if($project->getMedia('before_photos')->first())
                            <img src="{{ $project->getMedia('before_photos')->first()->getUrl('medium') }}" alt="{{ $project->name . '-before' }}" class="object-cover w-full ">
                        @endif
                    </div>

                    <div class="shadow-md rounded overflow-hidden">
                        @if($project->getMedia('after_photos')->first())
                            <img src="{{ $project->getMedia('after_photos')->first()->getUrl('medium') }}" alt="{{ $project->name . '-after' }}" class="object-cover w-full">
                        @endif
                    </div>
                </div>
                <p class="leading-none">{{ $project->name }}</p>
            </section>
        @endforeach
    </div>
    <div class="max-w-3xl mx-auto">
        <div class="bg-white py-3 flex items-center justify-between mb-6">
            <div class="sm:flex-1 sm:flex sm:items-center sm:justify-between">
                <div>
                    <p class="text-sm leading-5 text-gray-700">
                        Showing
                        <span class="font-medium"> {{ $projects->firstItem() }} </span>
                        to
                        <span class="font-medium"> {{ $projects->lastItem() }} </span>
                        of
                        <span class="font-medium">{{ $projects->total() }}</span>
                        results
                    </p>
                </div>

                {{ $projects->links('partials._pagination') }}

            </div>
        </div>
    </div>
</article>
