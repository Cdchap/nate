@section('heading', 'Projects')

<div class="flex flex-col">
    @if (session()->has('success'))
        @include('partials.messages._success')
    @endif
    <div class="flex flex-col items-start sm:flex-row sm:justify-between sm:items-center">
        <div class="flex-shrink-1 relative w-64">
            <span class="absolute inset-y-0 left-0 pl-3 flex items-center">
                <svg class="h-6 w-6 text-gray-600" fill="none" viewbox="0 0 24 24">
                    <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" stroke-linecap="round" stroke-width="2"
                          stroke="currentColor"/>
                </svg>
            </span>
            <input wire:model="search" type="text" placeholder="Search..." type="text"
                   class="focus:border-teal-600 block w-full rounded border border-gray-400 pl-10 pr-4 py-2 text-sm text-gray-900 placeholder-gray-600"/>
        </div>
        <div class="mt-4 sm:ml-0a sm:mt-4">
            <span class="relative z-0 inline-flex shadow-sm">
                <select wire:model="perPage"
                        class="-ml-px block form-select w-full pl-3 pr-9 py-2 rounded-md border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
                    <option value=10>10 per page</option>
                    <option value=25>25 per page</option>
                    <option value=50>50 per page</option>
                </select>
            </span>
        </div>
    </div>
    <a href="{{ route('project.create') }}" class="mt-4">
        <span class="inline-flex rounded-md shadow-sm">
            <span
                class="inline-flex items-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition ease-in-out duration-150">
                <x:heroicon-o-plus class="w-4 h-4 text-white mr-1"/>
                New Project
            </span>
        </span>
        <x:heroicon-o-plus class="w-4 h-4 text-white"/>
    </a>

    <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8 mt-4">
        <div class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
            <table class="min-w-full">
                <thead>
                <tr>
                    <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                        Name
                    </th>
                    <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                        Address
                    </th>
                    <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                        Description
                    </th>
                    <th class="px-6 py-3 border-b border-gray-200 bg-gray-50"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($projects as $project)
                    <tr class="even:bg-gray-200">
                        <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5 font-medium text-gray-900">
                            {{ $project->name }}
                        </td>
                        <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                            {{ $project->address }} {{ $project->city }} {{$project->state }} {{ $project->zip }}
                        </td>
                        <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                            {{ substr($project->description, 0, 20) }} @if (strlen($project->description) > 20) &hellip; @endif
                        </td>
                        <td class="px-6 py-4 whitespace-no-wrap flex justify-between text-sm leading-5 font-medium">
                            <a href="{{ route('project.show', $project) }}"
                               class="text-indigo-600 hover:text-indigo-900 focus:outline-none focus:underline">View</a>
                            <a href="{{ route('project.edit', $project) }}"
                               class="ml-2 text-red-600 hover:text-indigo-900 focus:outline-none focus:underline">Edit</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6">
        <div class="sm:flex-1 sm:flex sm:items-center sm:justify-between">
            <div>
                <p class="text-sm leading-5 text-gray-700">
                    Showing
                    <span class="font-medium"> {{ $projects->firstItem() }} </span>
                    to
                    <span class="font-medium"> {{ $projects->lastItem() }} </span>
                    of
                    <span class="font-medium">{{ $projects->total() }}</span>
                    results
                </p>
            </div>

            {{ $projects->links('partials._pagination') }}

        </div>
    </div>
</div>

