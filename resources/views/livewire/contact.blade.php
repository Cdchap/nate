<section class="vh90 max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
    <div class="max-w-3xl mx-auto grid grid-col-1 grid-rows-4">
        <h2 class="row-start-1 row-end-3 text-6xl font-bold tracking-tight mt-48 border-b">Contact</h2>
        <p class="row-start-3 row-end-4 text-xl sm:text-2xl font-light mt-16">
            Email &xrarr; nate@nextdoornate.com
        </p>
        <p class="text-xl sm:text-2xl font-light">
            Phone &xrarr; (734) 555-5555
        </p>
    </div>
</section>
