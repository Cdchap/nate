<div :class="{'block': open, 'hidden': !open}" class="hidden border-b border-gray-700 md:hidden">
    <div class="px-2 py-3 sm:px-3">
        <a href="{{ route('dashboard') }}" class="block px-3 py-2 rounded-md text-base font-medium {{ request()->url() === route('dashboard') ? 'text-white bg-gray-900' : '' }} text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700">Dashboard</a>
        <a href="{{ route('project.index') }}" class="mt-1 block px-3 py-2 rounded-md text-base font-medium {{ request()->url() === route('project.index') ? 'text-white bg-gray-900' : '' }} text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700">Projects</a>
        <a href="{{ route('horizon.index') }}" class="mt-1 block px-3 py-2 rounded-md text-base font-medium text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700">Horizon</a>
    </div>
    <div class="pt-4 pb-3 border-t border-gray-700">
        <div class="flex items-center px-5">
            <div class="flex-shrink-0">
                <div class="h-10 w-10 rounded-full bg-teal bg-teal-400 flex flex-col justify-center items-center">
                    <h4 class="font-bold text-white text-xl">{{ $initial }}</h4>
                </div>
{{--                <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="" />--}}
            </div>
            <div class="ml-3">
                <div class="text-base font-medium leading-none text-white">{{ $name }}</div>
                <div class="mt-1 text-sm font-medium leading-none text-gray-400">{{ $email }}</div>
            </div>
        </div>
        <div class="mt-3 px-2" role="menu" aria-orientation="vertical" aria-labelledby="user-menu">
            <a href="#" class="block px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700" role="menuitem">Your Profile</a>
            <a href="#" class="mt-1 block px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700" role="menuitem">Settings</a>
            <a class="mt-1 block px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700"
                href="{{ route('logout') }}" role="menuitem" onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                Sign Out
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                style="display: none;">
                @csrf
            </form>
        </div>
    </div>
</div>
