@extends('layouts.admin')

@section('heading', 'Dashboard')

@section('content')

    <div>
        <h1>This page can be removed, don't think that a dashboard is necessary. Can add back in if the site gets more features</h1>
    </div>

@endsection
