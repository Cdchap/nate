<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('partials._head')
</head>
<body>
    <div id="app">
        <nav x-data="{ navShow: false }" class="absolute z-50 ml-12 mt-12">
            @include('partials._siteNav')
        </nav>
        <main class="">
            @yield('content')
        </main>
        <footer>
            @include('partials._footer')
        </footer>
    </div>
    @livewireScripts
</body>
</html>
