<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('partials._head')
</head>
<body>
<div>
    <div class="bg-gray-800 pb-32">
        <nav x-data="{ open: false }" @keydown.window.escape="open = false" class="bg-gray-800">

            @livewire('desktop-nav')

            @livewire('mobile-nav')

        </nav>
        <header class="py-10">
            <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <h1 class="text-3xl leading-9 font-bold text-white">
                    @yield('heading')
                </h1>
            </div>
        </header>
    </div>
    <main class="-mt-32">
        <div class="max-w-7xl mx-auto pb-12 px-4 sm:px-6 lg:px-8">
            <!-- Replace with your content -->
            <div class="bg-white rounded-lg shadow px-5 py-6 sm:px-6">
                @yield('content')
            </div>
            <!-- /End replace -->
        </div>
    </main>
</div>
@livewireScripts
</body>
</html>
